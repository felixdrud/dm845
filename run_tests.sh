#!/bin/bash

# Run this script with:
#   $ ./run_tests.sh 2>&1 | tee log.txt

KMERSTREAM_BIN="./kmerstream/KmerStream"
KMERGENIE_BIN="./kmergenie/kmergenie"
JELLYFISH_BIN="./jellyfish/bin/jellyfish"
MEMUSG_BIN="./memusg"

DATA_DIR_PREFIX="data/"

DATA_FILES="Bombus_impatiens_frag_1.fastq.gz
Bombus_impatiens_frag_2.fastq.gz
Human_chr14_frag_1.fastq.gz
Human_chr14_frag_2.fastq.gz
C0561_GAGTGG_L006_R2_001.fastq.gz
C0561_GAGTGG_L007_R1_001.fastq.gz
C0561_GAGTGGAT_L007_R1_001.fastq.gz
C0561_GAGTGGAT_L007_R2_001.fastq.gz
C0561_GAGTGG_L006_R1_001.fastq.gz"

DATA_FILES_BOMBUS="Bombus_impatiens_frag_1.fastq.gz
Bombus_impatiens_frag_2.fastq.gz"

DATA_FILES_HUMAN="Human_chr14_frag_1.fastq.gz
Human_chr14_frag_2.fastq.gz"

printf "###################################\n"
printf "######## Counting k-mers ##########\n"
printf "###################################\n"
printf "Starting time: `date`\n\n"

for D in $DATA_FILES_HUMAN
do
    printf "######## Datafile: $D\n\n"
    for K in 16 31 101
    do
        printf "######## K: $K\n\n"
#       KmerStream
        printf "### [KmerStream] $D k=$K\n"
        TIME_START=`date +%s%3N`
        "$MEMUSG_BIN" "$KMERSTREAM_BIN" -k "$K" -o kmerstream-"$D"-k"$K".txt "$DATA_DIR_PREFIX$D"
        TIME_STOP=`date +%s%3N`
        MILLISECONDS_ELAPSED=$((TIME_STOP - TIME_START))
        SECONDS_ELAPSED=`bc -l <<< ${MILLISECONDS_ELAPSED}/1000`
        printf "### [KmerStream] SECONDS_ELAPSED: $SECONDS_ELAPSED ### $D k=$K\n\n\n\n"

#       KmerGenie
        printf "### [KmerGenie] $D k=$K\n"
        TIME_START=`date +%s%3N`
        "$MEMUSG_BIN" "$KMERGENIE_BIN" "$DATA_DIR_PREFIX$D" --one-pass -k "$K" -l "$K" -s 1 -o kmergenie-"$D"-k"$K" -t 1 
        TIME_STOP=`date +%s%3N`
        MILLISECONDS_ELAPSED=$((TIME_STOP - TIME_START))
        SECONDS_ELAPSED=`bc -l <<< ${MILLISECONDS_ELAPSED}/1000`
        printf "### [KmerGenie] SECONDS_ELAPSED: $SECONDS_ELAPSED ### $D k=$K\n\n\n\n"

#       Jellyfish
#       printf "### [Jellyfish] $D k=$K\n"
#       TIME_START=`date +%s%3N`
#       "$MEMUSG_BIN" "$JELLYFISH_BIN" count -m $K -s 100M -o jellyfish-"$D"-k"$K".jf <(zcat "$DATA_DIR_PREFIX$D")
#       TIME_STOP=`date +%s%3N`
#       MILLISECONDS_ELAPSED=$((TIME_STOP - TIME_START))
#       SECONDS_ELAPSED=`bc -l <<< ${MILLISECONDS_ELAPSED}/1000`
#       printf "### [Jellyfish] SECONDS_ELAPSED: $SECONDS_ELAPSED ### $D k=$K\n\n\n\n"
    done
done
printf "Ending time: `date`\n"